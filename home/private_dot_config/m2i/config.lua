config = {
    script = 'akaimpkmini2.lua',
    loglevel = 2,
    use_alsa = true,
    use_jack = false,
    reconnect = true,
    loop_enabled = true,
    main_freq = 250,
    loop_freq = 1000,
    watch_freq = 2500,
}
