;;; user-posthook.el --- user defined posthooks -*- lexical-binding: t -*-
;;; Commentary:
;;; Put your posthooks here, to be loaded after the rest of packages, right before settings
;;; Code:

(provide 'user-posthook)
;;; user-posthook.el ends here
