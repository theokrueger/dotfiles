;;; user-prehook.el --- user defined prehooks -*- lexical-binding: t -*-
;;; Commentary:
;;; Put your prehooks here, to be loaded before anything else
;;; Code:

(provide 'user-prehook)
;;; user-prehook.el ends here
