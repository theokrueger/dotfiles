;;; cxx-mode-init.el --- c/c++/cxx settings -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(setq-default
  c-basic-offset 8        ;; 8space tabs
  )

(provide 'cxx-mode-init)
;;; cxx-mode-init.el ends here
