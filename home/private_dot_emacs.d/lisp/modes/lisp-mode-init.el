;;; lisp-mode-init.el --- settings for elisp -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(setq-default lisp-indent-offset 2) ;; lisp 2space indent

(provide 'lisp-mode-init)
;;; lisp-mode-init.el ends here
