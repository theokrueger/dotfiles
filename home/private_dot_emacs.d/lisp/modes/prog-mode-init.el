;;; prog-mode-init.el --- settings for all programming modes -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; indentation settings
(setq-default
  indent-tabs-mode nil
  indent-line-function 'insert-tab
  tab-stop-list (number-sequence 8 160 8)
  tab-width 8
  standard-indent 8)
(global-set-key (kbd "RET") 'newline-and-indent)

;; rainbow delimiters
(require-package 'rainbow-delimiters)
(use-package rainbow-delimiters
  :defer t
  :commands rainbow-delimiters-mode
  :init
  (show-paren-mode 1) ;; highlight matching delimiters
  :hook (prog-mode . rainbow-delimiters-mode)
  )

;; indent highlighting
(require-package 'highlight-indent-guides)
(use-package highlight-indent-guides
  :defer t
  :commands highlight-indent-guides-mode
  :hook (prog-mode . highlight-indent-guides-mode)
  )

;; symbol overlays
(require-package 'symbol-overlay)
(use-package symbol-overlay
  :defer t
  :commands symbol-overlay-mode
  :hook (prog-mode . symbol-overlay-mode)
  :bind (
          ("M-i" . symbol-overlay-put)
          ("M-n" . symbol-overlay-switch-forward)
          ("M-p" . symbol-overlay-switch-backward)
          )
  )

;; shift lines up and down
(require-package 'move-dup)
(use-package move-dup
  :defer t
  :commands (
              move-dup-move-lines-up move-dup-move-lines-down
              )
  :bind (
          ("M-<up>"   . move-dup-move-lines-up)
          ("M-<down>" . move-dup-move-lines-down)
          )
  )

;; folding
(use-package hideshow
  :defer t
  :hook (prog-mode . hs-minor-mode)
  :bind-keymap
  ("C-c f" . hs-toggle-hiding)
  ("C-c F" . hs-hide-all)
  )


(provide 'prog-mode-init)
;;; prog-mode-init.el ends here
