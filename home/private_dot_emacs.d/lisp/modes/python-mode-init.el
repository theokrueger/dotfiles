;;; python-mode-init.el --- settings related to python programming mode -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:
(setq-default
  ;; 8 space tabs
  python-indent-offset 8
  ;; python version
  pythol-shell-interpreter "python3")

(provide 'python-mode-init)
;;; python-mode-init.el ends here
