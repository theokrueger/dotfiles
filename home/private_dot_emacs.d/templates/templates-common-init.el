;;; templates-common-init.el --- load templates for various modes -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'org-latex-classes-init) ;; custom latex classes

(provide 'templates-common-init)
;;; templates-common-init.el ends here
