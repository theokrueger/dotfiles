# sway-window-title-as-workspace
adds a named workspace that reflects the currently hovered window in sway

does so by creating an empty wayland window and sending it to a workspace, and constantly renaming said workspace on window change
